import json
from pprint import pprint

class CmdLoader():
	def __init__(self):
		pass

	def getCommands(self, file, path):
		cmds = self.registerCommands(file)
		commands = {}
		for cmd in cmds:
			commands = self.addJSONCommand(path + "\\" + cmd + ".json", commands)
		return commands
		
	def getParams(self, file, path):
		cmds = self.registerCommands(file)
		params = {}
		for cmd in cmds:
			params = self.addJSONParam(path + "\\" + cmd + ".json", params)
		return params
		
	def registerCommands(self, file):
		commands = []
		
		jsonData = open(file)
		data = json.load(jsonData)
		commands = data['files']
		return commands
		
	def addJSONCommand(self, file, list):
		jsonData = open(file)
		data = json.load(jsonData)
		list[data['command']] = data['function']
		
		return list
	
	def addJSONParam(self, file, list):
		jsonData = open(file)
		data = json.load(jsonData)
		if "infinite" in data['params']:
			list[data['command']] = -1
		else:
			list[data['command']] = data['params']
		
		return list