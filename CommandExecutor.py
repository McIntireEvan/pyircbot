import json

class CmdExecutor():
	def __init__(self):
		pass
		
	def runFunction(self, runnerClass, method):
		try:
			getattr(runnerClass, method)()
		except:
			print "Could not find function!"
			
	def runFunctionWithParams(self, runnerClass, method, params):
		try:
			getattr(runnerClass, method)(*params)
		except:
			print "Could not find function!(Params)"