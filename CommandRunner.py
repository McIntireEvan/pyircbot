import json

class CmdRunner():
	def __init__(self, irc, channel):
		self.irc = irc
		self.channel = channel
		
	def sendMessage(self, message, irc):
		irc.send('PRIVMSG %s :%s\n' % (self.channel, message))
	#Implement all your methods below

	def hello(self):
		self.sendMessage("Hello!",self.irc)

	def echo(self, *params):
		total = ""
		for param in params:
			total += param + " "
		self.sendMessage(total,self.irc)