import socket
import random
import ConfigParser
import json
import thread
import threading
import time
import os
from CommandExecutor import *
from CommandRegistrar import *
from CommandRunner import *

class ChatBot():
	def __init__(self):
		self.loadConfig()
		
		self.irc = socket.socket()
		self.irc.connect((self.server, 6667))
		
		self.connect(self.irc)
		
		self.commands = CmdLoader().getCommands(os.path.join(os.path.dirname(__file__),"commands.json"),os.path.dirname(__file__))
		self.params = CmdLoader().getParams(os.path.join(os.path.dirname(__file__),"commands.json"),os.path.dirname(__file__))
		self.runner = CmdRunner(self.irc, self.channel)
		
		self.inThread = threading.Thread(target = self.input_thread)
		self.inThread.start()
		self.L = []
		
		while True:
			time.sleep(.1)
			thread.start_new_thread(self.ircThreadRunner, ())
			if len(self.L) > 0: 
				self.inputMode()
				time.sleep(5)
				self.L = []	
				self.inThread = threading.Thread(target = self.input_thread)
				self.inThread.start()
				
	def ircThreadRunner(self):
		irc = self.irc
		line = irc.recv(2048)
		line = line.strip("\r\n")
		line = line.lower()
		lineSplit = line.split(':')
			
		print line
		
		lastWords = self.listToString(lineSplit[len(lineSplit) - 1])
	
		lastWords = lastWords.split(" ")
		for cmd in self.commands:
			if "!" in lastWords[0]:
				if lastWords[0][1:] in cmd:
					if self.params[cmd] == -1:
						CmdExecutor().runFunctionWithParams(self.runner, cmd, lastWords[1:])						
					elif len(self.params[cmd]) == 0:
						if len(self.params[cmd]) == len(lastWords) - 1:
							CmdExecutor().runFunction(self.runner, cmd)
					else: 
						if len(self.params[cmd]) == len(lastWords) - 1:
								CmdExecutor().runFunctionWithParams(self.runner, cmd, lastWords[1:])
		
	def input_thread(self):
		raw_input()
		self.L.append("")
	
	def inputMode(self):
		i = raw_input(">")
		if "send" in i.split(" ")[0]:
			self.sendMessage(self.listToSentence(i.split(" ")[1:]), self.irc)
	
	def sendMessage(self, message, irc):
		irc.send('PRIVMSG %s :%s\n' % (self.channel, message))
	
	def listToString(self, list):
		r = ""
		for l in list:
			r += l
		return r
	
	def listToSentence(self, list):
		r = ""
		for l in list:
			r += l + " "
		return r
	
	def loadConfig(self):
		configParser = ConfigParser.RawConfigParser()
		configFilePath =  os.path.join(os.path.dirname(__file__),"config.cfg")
		configParser.read(configFilePath)
		self.bot_owner = configParser.get('info', "botname")
		self.nick = configParser.get('info', "botname")
		self.channel = configParser.get('info', "channel")
		self.server = configParser.get('info', "url")
		self.password = configParser.get('info', "pass")
		
	def connect(self, irc):
		irc.send('PASS ' + self.password + '\r\n')
		irc.send('USER ' + self.nick + ' 0 * :' + self.bot_owner + '\r\n')
		irc.send('NICK ' + self.nick + '\r\n')
		irc.send('JOIN ' + self.channel + '\r\n') 

		self.sendMessage("BOT ONLINE", irc)
	
ChatBot()